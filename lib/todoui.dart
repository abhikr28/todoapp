import 'package:flutter/material.dart';

import 'package:helloflutter/databaseHelper.dart';

class todoui extends StatefulWidget {
  @override
  _todouiState createState() => _todouiState();
}

class _todouiState extends State<todoui> {
  var listt = [];
  var taskPopupText = TextEditingController();
  bool validated = true;
  String errorMsgText = '';
  List<Widget> childrenCard = new List<Widget>();

  final dbHelper = DatabaseHelper.instance;

  @override
  void initState() {
    super.initState();
    getAllTasksFromDb();
  }

  void getAllTasksFromDb() async {
    List<Widget> tempChildrenCard = new List<Widget>();
    var tempList = [];
    var allrows = await dbHelper.queryAllRows();
    print(allrows.length);
    if (allrows.length > 0) {
      allrows.forEach((row) {
        tempList.add(row);
        tempChildrenCard.add(todocard(row));
      });
      setState(() {
        childrenCard = tempChildrenCard;
        listt = tempList;
      });
    }
  }

  void deleteItem(index) {
    dbHelper.delete(index);
    var indexToDelete = 0;
    var i;
    for (i = 0; i < listt.length; i++) {
      if (listt[i]['_id'] == index) {
        indexToDelete = i;
      }
    }
    setState(() {
      childrenCard.removeAt(indexToDelete);
      listt.removeWhere((item) => item['_id'] == index);
    });
  }

  Widget todocard(dynamic tasks) {
    return Card(
      elevation: 5.0,
      margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.0),
      child: Container(
          child: ListTile(
        title: Text(
          tasks['task_text'],
          style: TextStyle(fontSize: 20),
        ),
        trailing: Wrap(
          spacing: 0,
          children: <Widget>[
            // IconButton(
            //   icon: Icon(
            //     Icons.done,
            //   ),
            //   onPressed: () {
            //     //   _onDeleteItemPressed(index);
            //     print("$tasks");
            //   },
            // ),
            IconButton(
              icon: Icon(
                Icons.delete_outline,
              ),
              onPressed: () {
                deleteItem(tasks['_id']);
                // print(tasks);
                //   _onDeleteItemPressed(index);
              },
            ),
          ],
        ),
      )),
    );
  }

  void addNewTask(String text) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: text,
    };
    final id = await dbHelper.insert(row);
    var tempObj = {"_id": id, "task_text": text};
    setState(() {
      listt.add(tempObj);
      childrenCard.add(todocard(tempObj));
    });
  }

  void deleteAllTasks() {
    if (listt.isNotEmpty) {
      dbHelper.deleteAllRecords();
      setState(() {
        childrenCard = [];
        listt = [];
      });
    }
  }

  void showAlertDialog() {
    taskPopupText.text = ""; // clearing value whenever task popup opens
    showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                title: Text("Add Task", textAlign: TextAlign.center),
                content: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextField(
                        autofocus: true,
                        controller: taskPopupText,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                          errorText: validated ? null : errorMsgText,
                        ),
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 15.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment
                                  .center, //Center Row contents horizontally,
                              crossAxisAlignment: CrossAxisAlignment
                                  .center, //Center Row contents vertically,
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () {
                                    if (taskPopupText.text.isNotEmpty) {
                                      if (taskPopupText.text.length > 15) {
                                        print(
                                            "Enter task less than 15 characters");
                                        setState(() {
                                          errorMsgText =
                                              "Enter task less than 15 characters";
                                          validated = false;
                                        });
                                      } else {
                                        setState(() {
                                          validated = true;
                                        });
                                        addNewTask(taskPopupText.text);
                                        Navigator.pop(context);
                                      }
                                    } else {
                                      print("Please enter a Task");
                                      setState(() {
                                        errorMsgText = "Please enter a Task";
                                        validated = false;
                                      });
                                    }
                                  },
                                  child: const Text(
                                    'Submit',
                                    // style: TextStyle(fontSize: 20)
                                  ),
                                ),
                              ]))
                    ],
                  ),
                ));
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: showAlertDialog,
        child: Icon(Icons.add, color: Colors.white),
        backgroundColor: Colors.red,
      ),
      appBar: AppBar(
        title: Text(
          "Todo App",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.clear,
              color: Colors.white,
            ),
            onPressed: () {
              deleteAllTasks();
            },
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Container(
        margin: const EdgeInsets.only(top: 20),
        child: listt.length > 0
            ? ListView.builder(
                itemCount: listt.length,
                itemBuilder: (context, int index) {
                  return todocard(listt[index]);
                },
              )
            : Center(
                child: Text(
                  'No Tasks',
                  style: TextStyle(fontSize: 20),
                ),
              ),
      ),
    );
  }
}
